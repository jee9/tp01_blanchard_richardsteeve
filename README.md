Quelles méthodes sont supportées par la servlet : 
		
	J'ai ajouter les Methode doGet, doPost, doPut et doDelete 

Que se passe t il lors des actions suivantes : GET, POST, PUT, DELETE

	GET = fait appelle a la methode doGet de la classe ExperimentServlet
			(Dans mon code je retourne une page Html avec le libelle : "Methode doGet for GET Call" )
			
	POST = fait appelle a la methode doPost de la classe ExperimentServlet
		(Dans mon code je retourne une page Html avec le libelle : "Methode doPost for POST Call" )
		
	PUT = fait appelle a la methode doPut de la classe ExperimentServlet
		(Dans mon code je retourne un JSON  : { "Description": "Methode doPut for PUT Call" } )
		
		curl --location --request PUT 'http://localhost:9090/TP01_Blanchard_RichardSteeve/experiment'	
		
	DELETE = fait appelle a la methode doDelete de la classe ExperimentServlet
		(Dans mon code je retourne un JSON  : { "Description": "Methode doDelete for DELETE Call" } )
		
		curl --location --request DELETE 'http://localhost:9090/TP01_Blanchard_RichardSteeve/experiment'

Ou se trouve le fichier etudiants.csv ? Savez-vous pourquoi?

	Le Fichier se trouve dans le repertoire du serveur d'application : Payara_Server\glassfish\domains\domain1\config
			Parce que c'est le repetoire par defaut si on ne specifie pas de repertoire