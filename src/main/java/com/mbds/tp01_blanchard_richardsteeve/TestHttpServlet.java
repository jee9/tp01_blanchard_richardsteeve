/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbds.tp01_blanchard_richardsteeve;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dell
 */
public class TestHttpServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TestHttpServlet</title>");            
            out.println("</head>");
            out.println("<body>");
             out.println("<a href=\"/TP01_Blanchard_RichardSteeve\">Retour</a>");
             out.println("<br>");
            out.println("<h1 style=\"text-align: center;\">Ma première servlet </h1>");
            
            //Information concernant l'adresse IP et le navigateur utilisé par le client
           
            out.println("<h2> Information concernant l'adresse IP et le navigateur utilisé par le client : </h2>"); 
            out.println("<table>");
            out.println("<tr> <td>Adresse IP du client :</td> <td>"+request.getRemoteAddr()+"</td> </tr>");
            out.println("<tr> <td>Navigateur du client :</td> <td>"+request.getHeader("User-Agent")+"</td> </tr>");
            out.println("<table>");
            
            // Information concernant la requete du client :
            out.println("<h2> Information concernant la requete du client : </h2>");
            out.println("<table>");
            out.println("<tr> <td>host :</td> <td>"+request.getMethod()+"</td> </tr>");
            out.println("<tr> <td>Protocole :</td> <td>"+request.getProtocol()+"</td> </tr>");
            out.println("<tr> <td>uri demandée :</td> <td>"+request.getRequestURI()+"</td> </tr>");
            out.println("<table>");
            
            //Information concernant l'entete de la requete :
            out.println("<h2> Information concernant l'entete de la requete : </h2>");
            out.println("<table>");
            out.println("<tr> <td>host :</td> <td>"+request.getHeader("host")+"</td> </tr>");
            out.println("<tr> <td>User-Agent :</td> <td>"+request.getHeader("User-Agent")+"</td> </tr>");
            out.println("<tr> <td>accept :</td> <td>"+request.getHeader("accept")+"</td> </tr>");
            out.println("<tr> <td>accept-language :</td> <td>"+request.getHeader("accept-language")+"</td> </tr>");
            out.println("<tr> <td>accept-encoding :</td> <td>"+request.getHeader("accept-encoding")+"</td> </tr>");
            out.println("<tr> <td>accept-charset :</td> <td>"+request.getHeader("accept-charset")+"</td> </tr>");
            out.println("<tr> <td>keep-alive :</td> <td>"+request.getContentLength()+"</td> </tr>");
            out.println("<tr> <td>connection :</td> <td>"+request.getHeader("connection")+"</td> </tr>");
            out.println("<table>");
            
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
