/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mbds.tp01_blanchard_richardsteeve;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dell
 */
public class EtudiantsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EtudiantsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EtudiantsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    protected void formulaireEtudiant(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EtudiantsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<a href=\"/TP01_Blanchard_RichardSteeve\">Retour</a>");
            out.println("<br>");
            out.println("<h1>Servlet Creation Etudiant </h1>");

            out.println("<form action=\"etudiants\" method=\"post\">");
            out.println("<table>");
            out.println("<tr> <td>Nom :</td> <td><input name=\"nom\" type=\"text\"/></td> </tr>");
            out.println("<tr> <td>Prenom :</td> <td><input name=\"prenom\" type=\"text\"/></td>  </tr>");
            out.println("<tr> <td>Email :</td> <td><input name=\"email\" type=\"text\"/></td>  </tr>");

            out.println("<table>");
            out.println("<input type=\"submit\" value=\"Valider\">");
            out.println("</form>");

            out.println("</body>");
            out.println("</html>");
        }
    }

    protected void listeEtudiant(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String row = "";
            String[] data = null;
          
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EtudiantsServlet</title>");
            out.println("</head>");
            out.println("<body >");
            out.println("<h1>Liste Etudiant </h1>");
            out.println("<table border=1>");
            out.println("<tr> <th>Nom </th> <th>Prenom </th> <th>Email </th> </tr>");

            File idea = new File("etudiant.csv");
            if (idea.exists()) {
                System.out.println(idea.getAbsolutePath());
                System.out.println(idea.getPath());
                BufferedReader csvReader = new BufferedReader(new FileReader(idea));
                while ((row = csvReader.readLine()) != null) {
                    data = row.split(",");
                    out.println("<tr> <td>" + data[0] + "</td> <td>" + data[1] + "</td> <td>" + data[2] + "</td> </tr>");
                }
            csvReader.close();
            }
            
            out.println("</table>");
            out.println("<br>");
            out.println("<a href=\"/TP01_Blanchard_RichardSteeve\">Retour</a>");
            out.println("</body>");
            out.println("</html>");
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        switch (request.getParameter("action")) {
            case "creation": {
                formulaireEtudiant(request, response);
                break;
            }

            case "liste": {
                listeEtudiant(request, response);
                break;
            }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        File idea = new File("etudiant.csv");
        FileWriter csvWriter = null;
        if (!idea.exists()) {
            csvWriter = new FileWriter(idea);
            csvWriter.append("Nom");
            csvWriter.append(",");
            csvWriter.append("Prenom");
            csvWriter.append(",");
            csvWriter.append("Email");
            csvWriter.append("\n");
        } else {
            csvWriter = new FileWriter(idea, true);
        }
        csvWriter.append(request.getParameter("nom"));
        csvWriter.append(",");
        csvWriter.append(request.getParameter("prenom"));
        csvWriter.append(",");
        csvWriter.append(request.getParameter("email"));
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();

        response.sendRedirect("etudiants?action=liste");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
